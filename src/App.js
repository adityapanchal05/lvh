import '../node_modules/bootstrap/dist/css/bootstrap.css';
import {Switch, Route} from 'react-router-dom';

import Signin from './Container/User/Form/Signin';
import Forgot from './Container/User/Form/Forgot';
import ResetPassword from './Container/User/Form/ResetPassword';
import Updated from './Container/User/Form/Updated';

import Account from './Container/User/Account/Account';
import AccCardView from './Container/User/Account/AccCardView';
import Footer from './Components/Footer';

import './App.css';

function App() {
  return (
    <div className="App">
        <Switch>
          {/* <Route exact path="/" component={Home}/> */}

          {/***********     User Form  *****************/}
          <Route exact path="/Signin" component={Signin}/>
          <Route exact path="/Forgot" component={Forgot}/>
          <Route exact path="/ResetPassword" component={ResetPassword}/>
          <Route exact path="/Updated" component={Updated}/>

          {/***********     User Account  *****************/}
          <Route exact path="/Account" component={Account}/>
          <Route exact path="/AccountCard" component={AccCardView}/>

          {/* <Route component={ErrorPage}/> */}
        </Switch>
        <Footer />
    </div>
  );
}

export default App;
