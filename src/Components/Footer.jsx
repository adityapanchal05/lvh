import React from 'react';
import { AppBar,makeStyles, Container, Toolbar, Typography } from '@material-ui/core';
import footer_svg from '../Images/Footer.svg';

const Footer = () => {

    const useStyles = makeStyles((theme) => ({
        footer: {
            backgroundColor: "#0B0B0B",
        },
        footer_text: {
            fontFamily: "Rubik",
            color: "#FFFFFF",
            fontSize: "0.75rem",
            margin: "auto",
            padding: 10,
        },
    }));

    const classes = useStyles();

    return (
    <>
    <AppBar position="static" className={classes.footer}>
        <Typography variant="body1" className={classes.footer_text}>
            <img src={footer_svg} alt="Logo image" />
            {/* CopyrightⒸ2021 LVHglobal. All right reserved. */}
        </Typography>
    </AppBar>
    </>
    )
}

export default Footer
