import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Controls from '../Components/Controls/Controls'

import img from '../Images/image.png';

const useStyles = makeStyles((theme) => ({
  root: {
    width: "17rem",
    height: "17rem",
    backgroundColor: "#121212",
  },
  more: {
    zIndex: 1
  },
  media: {
    height: "12.188rem",
    paddingTop: '56.25%', // 16:9
  },
  content: {
    color: "#FFFFFF",
    fontFamily: "Rubik",
    display: "flex",
    justifyContent: "space-between",
  },
  button: {
    width:"5.125rem",
    height:"1.813rem",
    fontSize:"0.688rem",
    textAlign: "Center",
    padding: "0rem"
  }
}));

export default function CardView() {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardMedia
        className={classes.media}
        image={img}
        // action={
        //   <IconButton aria-label="settings" className={classes.more}>
        //     <MoreVertIcon />
        //   </IconButton>
        // }
      />
      <CardContent className={classes.content} style={{fontSize: "16px"}}>
          <Typography gutterBottom>
            Massive Dynamic
            <Typography variant="body2" style={{fontSize: "14px", marginTop: "5px"}}>
              Vendor
            </Typography>
          </Typography>
          <Controls.OutlineButton type="submit" text="APPROVED" className={classes.button}/>
      </CardContent>
    </Card>
  );
}