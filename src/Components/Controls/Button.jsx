import React from 'react'
import { Button as MuiButton, makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        // margin: theme.spacing(0.5),
        width: "100%",
        fontFamily: "Rubik",
        backgroundColor: "#1C73E8",
        borderRadius: "0.125rem",
        textTransform: "uppercase",
        color: "#FFFFFF",
        fontSize: "0.875rem",
        height: "2.5rem",
        marginTop: "-1.875rem"
    },
    // label: {
    //     textTransform: 'none',
    // }
}))

export default function Button(props) {

    const { text, size, color, variant, onClick, ...other } = props
    const classes = useStyles();

    return (
        <MuiButton
            variant={variant || "contained"}
            onClick={onClick}
            {...other}
            classes={{ root: classes.root }}>
            {text}
        </MuiButton>
    )
}
