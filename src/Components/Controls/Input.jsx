import React from 'react'
import { InputBase, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: "#272727",
            width: "100%",
            marginTop: "0.290rem",
            boxSizing: "border-box",
            borderRadius: "2px",
            color: "#FFFFFF",
            height: "40px"
    },
}))

export default function Input(props) {

    const { name, label, value, onChange } = props;
    const classes = useStyles();

    return (
        <InputBase
            label={label}
            name={name}
            value={value}
            onChange={onChange}
            classes={{ root: classes.root }}
            inputProps={{ 'aria-label': 'naked' }}
      />
    )
}
