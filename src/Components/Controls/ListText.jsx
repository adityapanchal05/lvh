import React from 'react'
import { InputBase, ListItemText, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        color: "#8D8D8D",
        fontSize: "0.875rem",
        fontFamily: "Rubik",
        display: "flex",
        alignItems: "center",
    },
}))

export default function ListText(props) {

    const { primary } = props;
    const classes = useStyles();

    return (
        <ListItemText
            primary={primary}
            classes={{ root: classes.root }}
      />
    )
}
