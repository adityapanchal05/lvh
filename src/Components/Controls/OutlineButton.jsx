import React from 'react'
import { Button as MuiButton, makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    root: {
        // width: "5.125rem",
        // height: "1.813rem",
        fontSize: "0.688rem",
        letterSpacing: "0.063rem",
        display: "inline-block",
        borderRadius: "0.313rem",
        transition: "0.5s",
        color: "#76DD8D",
        border: "2px solid #76DD8D",
        textDecoration: "none",
        textTransform: "uppercase",
        backgroundColor: "#121212",
        // textAlign: "Center"
    },
}))

export default function OutlineButton(props) {

    const { text, size, color, variant, onClick, ...other } = props
    const classes = useStyles();

    return (
        <MuiButton
            variant={variant || "contained"}
            onClick={onClick}
            {...other}
            classes={{ root: classes.root }}>
            {text}
        </MuiButton>
    )
}
