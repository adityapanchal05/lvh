import Input from "./Input";
import Button from "./Button";
import ListText from "./ListText";
import OutlineButton from "./OutlineButton";

const Controls = {
    Input,
    Button,
    ListText,
    OutlineButton

}

export default Controls;