import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Controls from '../Components/Controls/Controls'


import SortingIcon from '../Images/SortingIcon.svg';
import MoreMenu from '../Images/MoreMenu.svg';
import { IconButton } from '@material-ui/core';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
    // borderRadius: "2px",
    backgroundColor: "#1B1B1B",
  },
  tableRow: {
    backgroundColor: "#1B1B1B",
  },
  tableCell: {
    fontSize: "0.75rem",
    fontFamily: "Rubik",
    color: "#8D8D8D",
    border: "none",
    backgroundColor: "#1B1B1B",
    // width: "162px"
  },
});

function createData(accInfo, accType, category, subCategory, location, status) {
  return { accInfo, accType, category, subCategory, location, status };
}

const rows = [
  createData('Massive Dynamic', 'Vendor', 'Chef', 'Pastry chef', '3517 W. Gray St. Utica, Pennsylvania 578...', 'APPROVED'),
  createData('Massive Dynamic', 'Vendor', 'Chef', 'Pastry chef', '3517 W. Gray St. Utica, Pennsylvania 578...', 'APPROVED'),
  createData('Massive Dynamic', 'Vendor', 'Chef', 'Pastry chef', '3517 W. Gray St. Utica, Pennsylvania 578...', 'APPROVED'),
  createData('Massive Dynamic', 'Vendor', 'Chef', 'Pastry chef', '3517 W. Gray St. Utica, Pennsylvania 578...', 'APPROVED'),
  createData('Massive Dynamic', 'Vendor', 'Chef', 'Pastry chef', '3517 W. Gray St. Utica, Pennsylvania 578...', 'APPROVED'),
  createData('Massive Dynamic', 'Vendor', 'Chef', 'Pastry chef', '3517 W. Gray St. Utica, Pennsylvania 578...', 'APPROVED'),
  createData('Massive Dynamic', 'Vendor', 'Chef', 'Pastry chef', '3517 W. Gray St. Utica, Pennsylvania 578...', 'APPROVED'),
  createData('Massive Dynamic', 'Vendor', 'Chef', 'Pastry chef', '3517 W. Gray St. Utica, Pennsylvania 578...', 'APPROVED'),
  createData('Massive Dynamic', 'Vendor', 'Chef', 'Pastry chef', '3517 W. Gray St. Utica, Pennsylvania 578...', 'APPROVED'),
  createData('Massive Dynamic', 'Vendor', 'Chef', 'Pastry chef', '3517 W. Gray St. Utica, Pennsylvania 578...', 'APPROVED'),
  createData('Massive Dynamic', 'Vendor', 'Chef', 'Pastry chef', '3517 W. Gray St. Utica, Pennsylvania 578...', 'APPROVED'),
];

export default function TableView() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table" >
        <TableHead>
          <TableRow className={classes.tableRow}>
            <TableCell className={classes.tableCell}>Account Information <img src={SortingIcon}/></TableCell>
            <TableCell className={classes.tableCell}>Account Type <img src={SortingIcon}/></TableCell>
            <TableCell className={classes.tableCell}>Category <img src={SortingIcon}/></TableCell>
            <TableCell className={classes.tableCell}>Sub-Category <img src={SortingIcon}/></TableCell>
            <TableCell className={classes.tableCell}>Location <img src={SortingIcon}/></TableCell>
            <TableCell className={clsx(classes.tableCell)}>Status <img src={SortingIcon}/></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.accInfo} style={{backgroundColor: "#121212"}}>
              <TableCell component="th" scope="row" style={{fontFamily: "Rubik", color:"#DFDFDF"}}>{row.accInfo}</TableCell>
              <TableCell style={{fontFamily: "Rubik", color:"#DFDFDF"}}>{row.accType}</TableCell>
              <TableCell style={{fontFamily: "Rubik", color:"#DFDFDF"}}>{row.category}</TableCell>
              <TableCell style={{fontFamily: "Rubik", color:"#DFDFDF"}}>{row.subCategory}</TableCell>
              <TableCell style={{fontFamily: "Rubik", color:"#DFDFDF"}}>{row.location}</TableCell>
              <TableCell><Controls.OutlineButton type="submit" text={row.status}/>
              <IconButton><img src={MoreMenu} alt={MoreMenu} /></IconButton></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}