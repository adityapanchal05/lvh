import React, { useState } from 'react';
import MenuIcon from '../Images/SideNav/Menu.svg';
import AccountImg from '../Images/SideNav/AccountMgmt.svg';
import AdministratorImg from '../Images/SideNav/Administrator.svg';
import DashboardImg from '../Images/SideNav/Dashboard.svg';
import FinanceImg from '../Images/SideNav/FinanceMgmt.svg';
import PersonImg from '../Images/SideNav/PersonMgmt.svg';
import ProductImg from '../Images/SideNav/ProductMgmt.svg';
import RequestImg from '../Images/SideNav/RequestMgmt.svg';
import Logo from '../Images/SideNav/Logo.svg';

import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import IconButton from '@material-ui/core/IconButton';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';

import Controls from './Controls/Controls';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  menuButton: {
    marginRight: 36,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    backgroundColor: "#0B0B0B",
    color: "#8D8D8D",
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    backgroundColor: "#121212",
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  icons: {
      marginLeft:"5px",
  },
}));

const SideNav = () => {

    const classes = useStyles();
    const [open, setOpen] = useState(false);
  
    const toggle = () => {
      open ? setOpen(false) : setOpen(true);
    }
    return (
    <>
        <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >

        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={toggle}
            edge="start"
          >
            <img src={MenuIcon} alt={MenuIcon}/>
          </IconButton>
        </Toolbar>
        <List>
            <ListItem>
                <ListItemIcon className={classes.icons}><img src={DashboardImg} alt={DashboardImg}/></ListItemIcon>
                <Controls.ListText primary="Dashboard" />
            </ListItem>
            <ListItem>
                <ListItemIcon className={classes.icons}><img src={AccountImg} alt={AccountImg}/></ListItemIcon>
                <Controls.ListText primary="Accounts" />
            </ListItem>
            <ListItem>
                <ListItemIcon className={classes.icons}><img src={PersonImg} alt={PersonImg}/></ListItemIcon>
                <Controls.ListText primary="Persons" />
            </ListItem>
            <ListItem>
                <ListItemIcon className={classes.icons}><img src={RequestImg} alt={RequestImg}/></ListItemIcon>
                <Controls.ListText primary="Requests" />
            </ListItem>
            <ListItem>
                <ListItemIcon className={classes.icons}><img src={ProductImg} alt={ProductImg}/></ListItemIcon>
                <Controls.ListText primary="Products" />
            </ListItem>
            <ListItem>
                <ListItemIcon className={classes.icons}><img src={FinanceImg} alt={FinanceImg}/></ListItemIcon>
                <Controls.ListText primary="Finances" />
            </ListItem>
            <ListItem>
                <ListItemIcon className={classes.icons}><img src={AdministratorImg} alt={AdministratorImg}/></ListItemIcon>
                <Controls.ListText primary="Administrator" />
            </ListItem>            
        </List>
      </Drawer>
    </>
    )
}

export default SideNav
