import React from 'react';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { makeStyles } from '@material-ui/core/styles';
import DownArrow from '../Images/Header/DownArrow.svg';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    color: "#FFFFFF"
  },
  paper: {
    marginRight: theme.spacing(2),
  },
  color: {
    color: "rgba(255, 255, 255, 0.8)",
    fontSize: "0.875rem",
    fontFamily: "Rubik",
    textTransform: "none",
    marginLeft: "-1.25rem"
  },
  DropDown: {
    width: "8.188rem",
    height: "6.375rem",
    backgroundColor: "#171717",
    // boxShadow: "0px 1px 3px rgba(0, 0, 0, 0.05)",
    borderRadius: "0.125rem",
    marginRight: "-80px"
  },
  text: {
    fontSize: "0.75rem",
    fontFamily: "Rubik",
    color: "#C0C0C0",
  }
}));

export default function Profile() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <div className={classes.root}>
        <Button
          ref={anchorRef}
          aria-controls={open ? 'menu-list-grow' : undefined}
          aria-haspopup="true"
          onClick={handleToggle}
          className={classes.color}
        >
          Margaret Washington <img src={DownArrow} alt={DownArrow}></img>
        </Button>
        <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown} className={classes.DropDown}>
                    <MenuItem onClick={handleClose} className={classes.text}>View Profile</MenuItem>
                    <MenuItem onClick={handleClose} className={classes.text}>Change Password</MenuItem>
                    <MenuItem onClick={handleClose} className={classes.text}>Sign Out</MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
    </div>
  );
}