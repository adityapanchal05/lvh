import React from 'react'
import { AppBar, Toolbar, Grid, IconButton, makeStyles, Typography, Avatar, Link } from '@material-ui/core'

import ListView from '../Images/Header/ListView.svg';
import CardView from '../Images/Header/CardView.svg';
import KanbanView from '../Images/Header/KanbanView.svg';
import Filter from '../Images/Header/Filter.svg';
import AddAccount from '../Images/Header/AddAccount.svg';
import Line from '../Images/Header/Line.svg';
import Search from '../Images/Header/Search.svg';
import Notification from '../Images/Header/Notification.svg';

import ProfilePic from '../Images/Profile/ProfilePic.svg';
import BaseLine from '../Images/Header/BaseLine.svg';

import Profile from './Profile';
import { NavLink } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: "#0B0B0B",
        boxShadow: "none"
    },
    HeaderText: {
        fontSize: "1rem",
        fontFamily: "Rubik",
        fontWeight: "700",
        textTransform: "uppercase",
    },
    headerIconL: {
        color: "red",
        width: "2.25rem",
        height: "2.25rem",
        marginLeft: "-0.938rem"
    },
    headerIconR: {
        color: "red",
        width: "2.375rem",
        height: "2.375rem",
        marginLeft: "-1.563rem"
    },
    small: {
        width: "1.875rem",
        height: "1.875rem",
        marginLeft: "-0.938rem"
      },
    baseLine: {
        margin: "auto",
        width: "97%"
    },
    view: {
        backgroundColor: "#121212",
        marginRight: "15px",
        display: "flex",
        justifyContent: "space-around",
        width: "110px",
        height: "36px",
    }
}));

export default function Header() {

    const classes = useStyles();

    return (
        <AppBar position="static" className={classes.root}>
            <Toolbar>
                <Grid container
                    alignItems="center">
                    <Grid item>
                    <Typography className={classes.HeaderText}>Accounts</Typography>
                    </Grid>
                    <Grid item sm></Grid>
                    <Grid item className={classes.view}>
                        <IconButton style={{padding:"0px"}} containerElement={<NavLink to="/signin" />} linkButton={true}>
                            <img src={ListView} alt={ListView} />
                        </IconButton>
                        <IconButton style={{padding:"0px"}}>
                            <img src={CardView} alt={CardView} />
                        </IconButton>
                        <IconButton style={{padding:"0px"}}>
                            <img src={KanbanView} alt={KanbanView} />
                        </IconButton>
                    </Grid>
                    <Grid item>
                        <IconButton>
                            <img src={Filter} alt={Filter} className={classes.headerIconL} />
                        </IconButton>
                        <IconButton>
                        <img src={AddAccount} alt={AddAccount} className={classes.headerIconL} />
                        </IconButton>
                        <IconButton>
                        <img src={Line} alt={Line} className={classes.headerIconL} />
                        </IconButton>
                        <IconButton>
                        <img src={Search} alt={Search} className={classes.headerIconR} />
                        </IconButton>
                        <IconButton>
                        <img src={Notification} alt={Notification} className={classes.headerIconR} />
                        </IconButton>
                        <IconButton>
                        <Avatar alt={ProfilePic} src={ProfilePic} className={classes.small} />
                        </IconButton>
                        <IconButton>
                        <Profile />
                        </IconButton>
                    </Grid>
                </Grid>
            </Toolbar>
            <img src={BaseLine} className={classes.baseLine}/>
        </AppBar>
    )
}
