import { Grid, makeStyles, Paper, Typography, } from '@material-ui/core';
import "typeface-cormorant";
import clsx from 'clsx';
import React from 'react'
import Main from './Main';
import Controls from '../../../Components/Controls/Controls';
import { Form } from '../../../Components/useForm';
import { NavLink } from 'react-router-dom';

    const useStyles = makeStyles((theme) => ({
        form: {
            fontFamily: "Rubik",
            backgroundColor: "#0B0B0B",
            color: "#FFFFFF",
            width: "33.125rem",
            height: "27.438rem",
            marginTop: "3.125rem",
            fontSize: "1.2rem",
            margin: "auto",
            fontWeight: "normal",
            padding: 35,
        },
        label: {
            marginTop: "1.0rem",
        },
        font13: {
            fontSize: "0.813rem",
            fontFamily: "Rubik",
            color: "#8D8D8D"
        },
        font14: {
            fontSize: "0.875rem",
            fontFamily: "Rubik",
            color: "#8D8D8D"
        },
        back_to_signin: {
            display: "flex",
            justifyContent: "center",
            color: "rgba(255, 255, 255, 0.8)",
            marginTop: "-0.2rem",
            marginBottom: "0.938rem",
        },
        button: {
            marginTop: "-0.875rem"
        }
    }));

    export default function ResetPassword() {
    const classes = useStyles();
    const handleSubmit = () => {
        
    }
    return (
    <>
    <div className="App-header">
        <Main />
        <Form onSubmit={handleSubmit}>
            <Grid>
                <Paper outlined={10} className={classes.form}>Reset Password
                <Typography className={clsx(classes.label, classes.font13)}>unique code<span className="mendatory">*</span></Typography>
                <Controls.Input
                        InputProps={{ classes }}
                        name="code"
                        // value={code}
                        // onChange={(e) => setCode(e.target.value)}
                />
                <Typography className={clsx(classes.label, classes.font13)}>new password<span className="mendatory">*</span></Typography>
                <Controls.Input
                        InputProps={{ classes }}
                        name="password"
                        // value={password}
                        // onChange={(e) => setPassword(e.target.value)}
                />
                <Typography className={clsx(classes.label, classes.font13)}>confirm password<span className="mendatory">*</span></Typography>
                <Controls.Input
                        InputProps={{ classes }}
                        name="cpassword"
                        // value={cpassword}
                        // onChange={(e) => setCpassword(e.target.value)}
                />
                <br /><br />
                <Controls.Button type="submit" text="Update Password" className={classes.button}/>
                <br /><br />
                <Typography className={clsx(classes.back_to_signin, classes.font14)}>
                    <NavLink to = "/signin" className={classes.back_to_signin}>
                        Sign In
                    </NavLink>
                </Typography>
                </Paper>
            </Grid>
        </Form>
    </div>
    </>
    )
}