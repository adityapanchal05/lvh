import { Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import "typeface-cormorant";
import clsx from 'clsx';
import React from 'react'
import Main from './Main';
import Controls from '../../../Components/Controls/Controls';
import { Form } from '../../../Components/useForm';
// import { NavLink } from 'react-router-dom';

    const useStyles = makeStyles(() => ({
        form: {
            fontFamily: "Rubik",
            backgroundColor: "#0B0B0B",
            color: "#FFFFFF",
            width: "33.125rem",
            height: "12.813rem",
            marginTop: "3.125rem",
            fontSize: "1.2rem",
            margin: "auto",
            fontWeight: "normal",
            padding: 35,
        },
        label: {
            marginTop: "0.5rem",
        },
        font14: {
            fontSize: "0.875rem",
            fontFamily: "Rubik",
            color: "#DFDFDF"
        },
        button: {
            marginTop: "-1.375rem"
        }
    }));

    export default function Updated() {
    const classes = useStyles();
    const handleSubmit = () => {

    }
    return (
    <>
    <div className="App-header">
        <Main />
        <Form onSubmit={handleSubmit}>
            <Grid>
                <Paper outlined={10} className={classes.form}>Successful!
                <Typography className={clsx(classes.label, classes.font14)}>Congratulations, Your password has been updated.<br />
                Please Sign In with your new password.</Typography>
                <br />
                <Controls.Button type="submit" text="Back To Sign In" className={classes.button}/>
                </Paper>
            </Grid>
        </Form>
    </div>
    </>
    )
}