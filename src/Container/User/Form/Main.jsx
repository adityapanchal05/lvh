import React from 'react';
import logo from '../../../Images/logo.svg';

const Main = () => {
    return (
    <>
        <div className="signin-image">
            <figure>
                <img src={logo} alt="Logo image" />
            </figure>
        </div>
    </>
    )
}

export default Main
