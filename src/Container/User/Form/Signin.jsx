import { Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import "typeface-cormorant";
import clsx from 'clsx';
import React, { useState } from 'react';
import Main from './Main';
import Controls from '../../../Components/Controls/Controls';
import { Form } from '../../../Components/useForm';
import { NavLink } from 'react-router-dom';

    const useStyles = makeStyles(() => ({
        signin_form: {
            fontFamily: "Rubik",
            backgroundColor: "#0B0B0B",
            color: "#FFFFFF",
            width: "33.125rem",
            height: "21.063rem",
            marginTop: "3.125rem",
            fontSize: "1.2rem",
            margin: "auto",
            fontWeight: "normal",
            padding: 35,
        },
        label: {
            marginTop: "1.0rem",
        },
        font13: {
            fontSize: "0.813rem",
            fontFamily: "Rubik",
            color: "#8D8D8D"
        },
        font14: {
            fontSize: "0.875rem",
            fontFamily: "Rubik",
            color: "#8D8D8D"
        },
        forgot: {
            float: "right",
            color: "rgba(255, 255, 255, 0.8)",
            marginTop: "-0.75rem",
            marginBottom: "0.938rem",
        }
    }));

    export default function Signin() {

    const classes = useStyles();    

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();

        const res = await fetch('https://dev.api.lvhplatform.com/api/v2.2/users/login', {
            method : "POST",
            headers : {
                "Content-Type" : "application/json"
            },
            body: {
                "email" : email,
                "password" : password,
                "system" : "mobile",
                "version" : "1.3"
            }
        });
        const data = res.json();

        if(res.status === 400 || !data) {
            console.log("Invalid Credentials");
        }else{
            console.log("Login Successfull");
            console.log(`${email}, ${password}`);
        }
    }

    return (
    <>
    <div className="App-header">
        <Main />
        <Form onSubmit={handleSubmit}>
            <Grid>
                <Paper outlined={10} className={classes.signin_form}>Sign In
                <Typography className={clsx(classes.label, classes.font13)}>email address<span className="mendatory">*</span></Typography>
                <Controls.Input
                    InputProps={{ classes }}
                    name="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <Typography className={clsx(classes.label, classes.font13)}>password<span className="mendatory">*</span></Typography>
                <Controls.Input
                    InputProps={{ classes }}
                    name="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <br /><br />
                <Typography className={clsx(classes.forgot, classes.font14)}>
                    <NavLink to = "/forgot" className={classes.forgot}>
                        Forgot Password?
                    </NavLink>
                </Typography>
                <br />
                <Controls.Button type="submit" text="Sign In"/>
                </Paper>
            </Grid>
        </Form>
    </div>
    </>
    )
}
