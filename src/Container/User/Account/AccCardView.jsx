import React from 'react'
import SideNav from '../../../Components/SideNav';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Header from '../../../Components/Header';
import CardView from '../../../Components/CardView';

const useStyles = makeStyles((theme) => ({
    appMain: {
        width: '100%'
    },
    root: {
      display: 'flex',
      backgroundColor: '#0B0B0B'
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }));
  

const AccCardView = () => {
    const classes = useStyles();
    const theme = useTheme();
    return (
    <div className={classes.root}>
        <SideNav />
        <div className={classes.appMain}>
            <Header />
            <main className={classes.content}>
                <CardView />
            </main>
        </div>
    </div>
    )
}

export default AccCardView
